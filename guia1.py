#importamos gi
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

#creamos una clase para la ventana principal

class sumas():
    #creamos una funcion principal
    def __init__(self):
        #creamos un objeto que hara a instrospeccion 
        builder = Gtk.Builder()
        #llamamos al archivo creado
        builder.add_from_file("guia.ui")
        #asociamos atributos cada uno de los elementos del glade(ID) 
        ventana = builder.get_object("principal")
        #se inicia la ventana en pantalla completa
        ventana.maximize()
        #le damos un titulo a la ventana
        ventana.set_title("Sumador de datos")
        #nos aseguramos que al apretar la x esta ventana se cierre de manera segura
        ventana.connect("destroy", Gtk.main_quit)

        #creamos el objeto boton_aceptar de la ventana principal
        boton_aceptar = builder.get_object("boton_aceptar")
        #hacemos que realice una funcion haciendo click
        boton_aceptar.connect("clicked", self.abrir_ventana)
        #creamos el objeto boton_reset de la ventana principal
        boton_reset = builder.get_object("boton_reset")
        #hacemos que realice una funcion haciendo click
        boton_reset.connect("clicked", self.resetear)

        #creamos los objetos result, num1 y num2 los cuales son labels que guardaran los datos igresados
        #en los entrys
        self.result = builder.get_object("resultado")
        self.num1 = builder.get_object("num1")
        self.num2 = builder.get_object("num2")
         
        #creamos los objetos dato1, dao2 los cuales son los entrys y hacemos que 
        #realicen una funcion al presionar enter
        self.dato1 = builder.get_object("dato1")
        self.dato2 = builder.get_object("dato2")
        self.dato1.connect("activate", self.activar_suma)
        self.dato2.connect("activate", self.activar_suma)
        
        #mostramos la ventana   
        ventana.show_all()

    

#creamos una funcion la cual realiza la sumatoria 
    def activar_suma(self, enter=None):
        #variables para despues saber cuando seran palabras o numeros 
        primero = 0
        segundo = 0
        secundario = 0
        primario = 0
        #se le entrega el contenido de los entry para ocuparlos mas tarde 
        self.primario = self.dato1.get_text()
        self.secundario = self.dato2.get_text()
        #se les entrega el contenido a los label de los entrys
        self.num1.set_label(self.dato1.get_text()) 
        self.num2.set_label(self.dato2.get_text())
        #se les entrega a las variables el contenido de los labels 
        self.primero = self.num1.get_text()
        self.segundo = self.num2.get_text()
        
        #se revisa si el contenido de las variales es un numero o una palabra 
        if self.primero.isdigit():
            self.primero = int(self.primero)
        else:
            self.primero = len(self.primero)

        

        if self.segundo.isdigit():
            self.segundo = int(self.segundo)
        else:
            self.segundo = len(self.segundo)
        
        

        #el contenido de las variables se pasan a entero 
        self.primero = int(self.primero)
        self.segundo = int(self.segundo)
        #se leariza la suma entre las variables
        self.suma = self.primero + self.segundo
        self.suma = str(self.suma)
        
        #se le entrega un resultado al label para mostrarlo en la ventana
        self.result.set_label(self.suma)

    #esta funcion es para el boton limpiar el cual limpia toda la ventana 
    def resetear(self, btn=None):
        self.dato1.set_text("")
        self.dato2.set_text("")
        self.num1.set_text("")
        self.num2.set_text("")
        self.result.set_text("")



    #esta funcion habrira la ventana secundaria 
    def abrir_ventana(self, btn=None):
        ventana_nueva = NuevaVentana()
        #se les entraga los datos ingresados en al ventana principal a la ventana secundaria 
        ventana_nueva.Primer_dato.set_text(self.num1.get_text())
        ventana_nueva.Segundo_dato.set_text(self.num2.get_text())
        ventana_nueva.Dato_resultado.set_label(self.suma)

        #para ver la funcion de cada boton
        response = ventana_nueva.dialogo.run()

        #se revisan las diferentes opciones que puedan ocurrir 
        #si es que ambos son enteros 
        if self.primario.isdigit() and self.secundario.isdigit():   
            #si se apreta el boton ok se borra todo  
            if response == Gtk.ResponseType.OK:
                self.dato1.set_text("0")
                self.dato2.set_text("0")
                self.num1.set_label("0")
                self.num2.set_label("0")
                self.result.set_label("0")
            #si se apreta el boton cancelar se vuelve a la ventana principal     
            elif response == Gtk.ResponseType.CANCEL:
                pass
        #si es que el primero es una palabra y el segundo un numero         
        elif self.primario.isalpha() and self.secundario.isdigit(): 
            #si se apreta el boton ok se borra todo     
            if response == Gtk.ResponseType.OK:
                self.dato1.set_text("")
                self.dato2.set_text("0")
                self.num1.set_label("")
                self.num2.set_label("0")
                self.result.set_label("0")
            #si se apreta el boton cancelar se vuelve a la ventana principal         
            elif response == Gtk.ResponseType.CANCEL:
                pass
        #si es que el primero es un numero y el segundo una palabra        
        if self.primario.isdigit() and self.secundario.isalpha():  
            #si se apreta el boton ok se borra todo    
            if response == Gtk.ResponseType.OK:
                self.dato1.set_text("0")
                self.dato2.set_text("")
                self.num1.set_label("0")
                self.num2.set_label("")
                self.result.set_label("0")
            #si se apreta el boton cancelar se vuelve a la ventana principal         
            elif response == Gtk.ResponseType.CANCEL:
                pass
        #si es que ambas son palabras        
        if self.primario.isalpha() and self.secundario.isalpha():    
            #si se apreta el boton ok se borra todo  
            if response == Gtk.ResponseType.OK:
                self.dato1.set_text("")
                self.dato2.set_text("")
                self.num1.set_label("")
                self.num2.set_label("")
                self.result.set_label("0")
            #si se apreta el boton cancelar se vuelve a la ventana principal         
            elif response == Gtk.ResponseType.CANCEL:
                pass
       #la ventana secundaria se cierra automaticamente al presionar un boton 
        ventana_nueva.dialogo.destroy()
        


#creamos una clase para la ventana secundaria 
class NuevaVentana():
    #se crea la funcion principal de esta clase 
    def __init__(self):
        #creamos un objeto que hara a instrospeccion
        builder = Gtk.Builder()
        #llamamos al archivo creado
        builder.add_from_file("guia.ui")

        #asociamos atributos cada uno de los elementos del glade(ID)
        self.dialogo = builder.get_object("nueva_ventana")
        
        #se crean objetos que contengan los entrys y el label 
        self.Primer_dato = builder.get_object("Primer_dato")
        self.Segundo_dato = builder.get_object("Segundo_dato")
        self.Dato_resultado = builder.get_object("Dato_resultado")

        
        #se crea objetos que contengan los botones de esta ventana
        boton_aceptar1 = builder.get_object("boton_aceptar1")
        boton_cancelar = builder.get_object("boton_cancelar")
        #hacemos que realicen una funcion al hacer click
        boton_aceptar1.connect("clicked", self.reseteo)
        boton_cancelar.connect("clicked", self.volver)
        
        #mostramos la ventan 
        self.dialogo.show_all()

    #se crea una funcion de reseteo para el boton aceptar
    def reseteo(self, btn=None):
        self.Primer_dato.set_text("0")
        self.Segundo_dato.set_text("0")
        self.Dato_resultado.set_label("0")
       

    #se crea una funcion para el boton cancelar el cual vuelve a la ventana principal 
    def volver(self, btn=None):
        self.dialogo.destroy()




if __name__ == "__main__":
    #se llama 
    sumas()
    Gtk.main()


